;;; 10-16-2017 replaced dependencies.lisp with :feature
;;; 10-15-2017 added cs325 (for update-file)
;;; 09-17-2014 dropped clp-exts [CKR]

(asdf:defsystem #:cs325
    :serial t
  :depends-on (#:lisp-unit)
  :components ((:file "package")
               (:file "tables")
               (:file "extend-match")
               (:file "lisp-critic")
               (:file "mops")
               (:file "ddr")
               (:file "exercise-tests")
               (:file "lisp-rules")
               (:file "ddr-tests")
               (:file "ddr-exs-tests")
               ))
